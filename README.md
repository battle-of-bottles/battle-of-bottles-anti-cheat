# Battle of Bottles anti-cheat

This is a server-side anti-cheat for Battle of Bottles that monitors all player actions and checks their validity.

### Installation
Download the anti-cheat mod [here](https://battle-of-bottles.itch.io/battle-of-bottles-anti-cheat) and place it in the server's **mods** directory. Edit the **config.json** file in the server directory and add **"anti-cheat.pck"** to **mods**. Restart the server and the anti-cheat will be automatically loaded.

### Exporting
Open the project in the [Godot editor](https://godotengine.org/). Go to **Project > Export** and add an export preset. Go to the "Resources" tab and add
"*.json" as a non-resource file that should be exported. Finally, press **Export PCK/Zip** and name the mod anti-cheat.pck.
