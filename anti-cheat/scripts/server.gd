extends Node

# This script will overwrite the main server script when the mod gets loaded.

const SAVE_PATH_SETTINGS = "config.json"
const PROTOCOL_VERSION = "1.3-dev"
const DEFAULT_SETTINGS = {
	"server_name": "Bottle Server",
	"port": 25575,
	"max_players": 16,
	"map": "default",
	"check_mods": true,
}

var simulation

var settings = DEFAULT_SETTINGS

var bottle_master
var zombie_master

var player_info = {}
var my_info = { "name": "_server" }

var server_info = {"name": "Bottle Server"}
var loaded_mods = []
var broadcast = false
var broadcast_interval = 1.0
var socket_udp
var broadcast_port = 25576
var time = 0

onready var broadcast_timer = $Timer


func _ready():
	load_settings()
	var dir = Directory.new()
	if not dir.dir_exists(Mods.MODS_PATH):
		dir.make_dir(Mods.MODS_PATH)
	
	var file = File.new()
	for mod in get_files(Mods.MODS_PATH):
		if file.file_exists(Mods.MODS_PATH + mod):
			if not mod == "anti-cheat.pck":
				loaded_mods.append(mod)
				Mods.load_mod(mod)
		else:
			push_error("%s could not be loaded" % mod)
	
	# Terminate previously created server
	get_tree().network_peer = null
	
	# Load simulation scene
	var simulation_scene = load("res://anti-cheat/scenes/simulation.tscn")
	simulation = simulation_scene.instance()
	add_child(simulation)
	simulation.load_map(settings["map"])
	
	# Create a new server
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(settings["port"], settings["max_players"])
	get_tree().network_peer = peer
	enable_broadcast()

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	print("Anti-cheat is running.")


func load_settings():
	var file = File.new()
	if file.file_exists(SAVE_PATH_SETTINGS):
		file.open(SAVE_PATH_SETTINGS, File.READ)
		var raw_data = file.get_as_text()
		var file_settings = parse_json(raw_data)
		if compare_arrays(DEFAULT_SETTINGS.keys(), file_settings.keys()):
			settings = file_settings
			server_info["name"] = settings["server_name"]
		else:
			print("Invalid config detected!")
			save_settings()
	else:
		save_settings()


func save_settings():
	var file = File.new()
	file.open(SAVE_PATH_SETTINGS, File.WRITE)
	file.store_string(to_json(settings))
	file.close()


func list_settings():
	for i in settings.keys():
		print(i + ": " + str(settings[i]))


func compare_arrays(arr1: Array, arr2: Array) -> bool:
	for i in arr1:
		if not arr2.has(i):
			return false
	return true


func get_files(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin(true)
	var file = dir.get_next()
	while file != "":
		files += [file]
		file = dir.get_next()
	return files


func _player_connected(id):
	if bottle_master == null:
		bottle_master = id
		rpc_id(bottle_master, "set_bottle_master", true)
		print("new bottle master: " + str(bottle_master))
		
	if zombie_master == null:
		zombie_master = id
		rpc_id(zombie_master, "set_zombie_master", true)
		print("new zombie master: " + str(bottle_master))

	rpc_id(id, "register_player", my_info, [0, 0])
	rpc_id(id, "set_map", settings["map"])
	rpc_id(id, "protocol_version_check", PROTOCOL_VERSION)
	if settings["check_mods"]:
		# Do not tell clients about anti-cheat
		rpc_id(id, "mods_check", loaded_mods)
	print(str(id) + " connected:")


remote func protocol_version_check(version):
	pass


remote func mods_check(client_mods):
	pass


func _player_disconnected(id):
	player_info.erase(id) # Erase player from info.
	if id == bottle_master:
		if player_info.size() > 0:
			bottle_master = player_info.keys()[0]
			rpc_id(bottle_master, "set_bottle_master", true)
			print("new bottle master: " + str(bottle_master))
		else:
			bottle_master = null
	if id == zombie_master:
		if player_info.size() > 0:
			zombie_master = player_info.keys()[0]
			rpc_id(zombie_master, "set_zombie_master", true)
			print("new zombie master: " + str(zombie_master))
		else:
			zombie_master = null
	simulation.remove_player(id)
	
	print(str(id) + " disconnected")


remote func register_player(info, stats):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	
	# Store the info
	var player_instance = null
	player_info[id] = {
		"info": info,
		"instance": player_instance,
		"stats": stats
	}
	
	simulation.add_player(id)


remote func update_position(position, rot):
	simulation.update_position(get_tree().get_rpc_sender_id(), position, rot)


remote func other_shoot(pos):
	simulation.shoot(get_tree().get_rpc_sender_id(), pos)


remote func other_reload():
	simulation.reload(get_tree().get_rpc_sender_id())


remote func other_jump():
	simulation.jump(get_tree().get_rpc_sender_id())


remote func player_dead(pos: Vector3, rot: Vector3, bullet_global_transform: Transform, killer_id: int):
	simulation.player_dead(get_tree().get_rpc_sender_id())


remote func other_bottle_hit(name, damage, bullet_global_trans):
	pass


remote func respawn():
	simulation.respawn(get_tree().get_rpc_sender_id())


remote func other_change_weapon(weapon):
	simulation.change_weapon(get_tree().get_rpc_sender_id(), weapon)


remote func hit(id, damage, bullet_global_trans):
	simulation.hit(get_tree().get_rpc_sender_id(), id, damage, bullet_global_trans)


remote func other_map_loaded():
	pass


remote func other_zombie_hit(id, damage, bullet_global_trans):
	pass


remote func other_zombie_die(id, bullet_global_transform):
	pass


remote func other_new_zombie(name, target_id, id, origin):
	pass


remote func other_zombie_sync(id, origin):
	pass


remote func other_player_added():
	pass

func enable_broadcast():
	broadcast = true
	broadcast_timer.start()
	socket_udp = PacketPeerUDP.new()
	socket_udp.set_broadcast_enabled(true)
	socket_udp.set_dest_address('255.255.255.255', broadcast_port)


func disable_broadcast():
	broadcast = false
	broadcast_timer.stop()
	if socket_udp != null:
		socket_udp.close()


func broadcast():
	if broadcast:
		server_info["player_count"] = player_info.keys().size()
		var packet_message = to_json(server_info)
		var packet = packet_message.to_ascii()
		socket_udp.put_packet(packet)


func _exit_tree():
	broadcast_timer.stop()
	if socket_udp != null:
		socket_udp.close()


func _physics_process(delta):
	time += delta


func kick_player(id):
	get_tree().network_peer.disconnect_peer(id)
