extends Spatial


const MAP_PATH = "res://anti-cheat/scenes/maps/"
const PLAYER_SCENE = preload("res://anti-cheat/scenes/player.tscn")

var current_map = null
var players = {}

onready var players_node = $players


func _ready():
	print("Starting simulation.")


func load_map(map_name):
	if current_map != null:
		current_map.queue_free()
	if map_name == "default":
		var file = load(MAP_PATH + "Map.tscn")
		current_map = file.instance()
		add_child(current_map)
	if map_name == "zombie_map":
		var file = load(MAP_PATH + "Map3.tscn")
		current_map = file.instance()
		add_child(current_map)


func add_player(id):
	var player_instance = PLAYER_SCENE.instance()
	player_instance.name = str(id)
	players_node.add_child(player_instance)
	player_instance.player_id = id
	player_instance.connect("kick_player", self, "kick_player")
	players[id] = player_instance


func remove_player(id):
	if players.has(id):
		var player = players[id]
		player.queue_free()
		players.erase(id)


func update_position(player_id, new_pos, new_rot):
	if players.has(player_id):
		players[player_id].update_position(new_pos, new_rot)


func shoot(player_id, pos):
	if players.has(player_id):
		players[player_id].shoot(pos)


func jump(player_id):
	if players.has(player_id):
		players[player_id].jump()


func reload(player_id):
	if players.has(player_id):
		players[player_id].reload()


func hit(player_id, id, damage, bullet_global_trans):
	if players.has(player_id):
		players[player_id].hit(damage)
		check_hit(player_id, id, bullet_global_trans)


func check_hit(killer_id, victim_id, bullet: Transform):
	var killer: KinematicBody = players[killer_id]
	var victim: KinematicBody = players[victim_id]
	
	var ray: RayCast = victim.ray
	var shoot_pos: Vector3 = killer.shoot_pos
	var vtranslation: Vector3 = victim.translation
	
	var rdiff = (Vector2(shoot_pos.x, shoot_pos.z) - Vector2(vtranslation.x, vtranslation.z)).length()
	if rdiff > victim.RADIUS * (1 / victim.DIFF_REL):
		killer.suspicious_activity(killer.check.SHOOT)
	
	var hdiff = abs(shoot_pos.y - vtranslation.y)
	if hdiff > victim.HEIGHT * (1 / victim.DIFF_REL):
		killer.suspicious_activity(killer.check.SHOOT)
	
	ray.global_transform.origin = killer.shoot_pos
	ray.look_at(killer.get_node("Head").global_transform.origin, Vector3.UP)
	ray.force_raycast_update()
	if ray.get_collider():
		if ray.get_collider() != killer:
			killer.suspicious_activity(killer.check.SHOOT)
	else:
		killer.suspicious_activity(killer.check.SHOOT)


func player_dead(player_id):
	if players.has(player_id):
		players[player_id].die()


func respawn(player_id):
	if players.has(player_id):
		players[player_id].respawn()


func change_weapon(player_id, weapon):
	if players.has(player_id):
		players[player_id].change_weapon(weapon)


func kick_player(id):
	get_node("/root/Multiplayer").kick_player(id)
