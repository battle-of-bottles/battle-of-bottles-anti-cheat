extends KinematicBody

signal kick_player

const GRAVITY = -24.8
const JUMP_POWER = 10
const MAX_HORIZONTAL_SPEED = 15
const SUSPICION_RESET_TIME = 10
const DIFF = 3
const FLOOR_DIFF = 0.25
const DIFF_REL = 0.75
const LAST_DELTA_COUNT = 10
const SG_BULLETS = 10

const HEIGHT = 1.8
const RADIUS = 0.63

var shoot_pos = Vector3.ZERO
enum check {VERTICAL_MOVEMENT, HORIZONTAL_MOVEMENT, GUN, SHOOT}

var player_id = 0
var last_update = 0
var last_normal_shot = -1
var last_shot = -1       # -1 instead of 0 to prevent shooting multiple times at once (as it turns 
var current_shotgun = -1 # out this is possible)
var last_hit = -1
var suspicion_reset_delta = 0
var suspicious_behaviour = {
	check.VERTICAL_MOVEMENT : 0,
	check.HORIZONTAL_MOVEMENT : 0,
	check.GUN : 0,
	check.SHOOT : 0,
}
var max_suspicion = {
	check.VERTICAL_MOVEMENT : 30,
	check.HORIZONTAL_MOVEMENT : 5,
	check.GUN : 3,
	check.SHOOT : 4,
}
var last_deltas = []
var vertical_velocity = 0
var alive = true

# weapon activity
var current_weapon = "machinegun"
var reload_weapon = "machinegun"
var guns = {
	"machinegun": [30, 30, false, true, 0.1, 0.5, 10],
	"shotgun": [6, 6, false, false, 1.35, 3.0, 10],
	"sniper": [10, 10, true, false, 2.4, 3.0, 100],
}
var last_cooldown = 0

var current_sg_bullets = SG_BULLETS

onready var ray = $HitChecker


func _physics_process(delta):
	# Reset suspicion every 10 seconds
	suspicion_reset_delta += delta
	if suspicion_reset_delta > SUSPICION_RESET_TIME:
		suspicion_reset_delta = 0
		for i in suspicious_behaviour.keys():
			suspicious_behaviour[i] = 0
	# Calculate vertical velocity
	if not get_floor_disctance() < FLOOR_DIFF:
		vertical_velocity += delta * GRAVITY
	elif vertical_velocity != JUMP_POWER:
		vertical_velocity = 0


func update_position(new_pos: Vector3, new_rot: Vector3):
	var time_now = get_time()
	var delta = time_now - last_update
	if last_update == 0:
		delta = 0
	last_update = time_now
	var old_pos: Vector3 = translation
	move_and_slide(new_pos - translation + Vector3(0, -0.1, 0), Vector3.UP, true, 4, deg2rad(80), false)
	translation = new_pos
	rotation = new_rot
	last_deltas.append(delta)
	if last_deltas.size() > LAST_DELTA_COUNT:
		last_deltas.remove(0)
	if delta != 0 and alive and last_deltas.size() >= LAST_DELTA_COUNT:
		movement_check(old_pos, new_pos, delta, last_deltas)


func change_weapon(weapon):
	if guns.has(weapon):
		current_weapon = weapon
	else:
		kick_player()


func jump():
	vertical_velocity = JUMP_POWER


func shoot(pos):
	shoot_pos = pos
	var time_now = get_time()
	last_normal_shot = time_now
	if current_weapon == "shotgun":
		if current_sg_bullets == 10:
			last_shot = current_shotgun
			current_shotgun = get_time()
			last_cooldown = guns[current_weapon][4] * 1000 * DIFF_REL
		current_sg_bullets -= 1
		if current_sg_bullets <= 0:
			current_sg_bullets = SG_BULLETS
		else:
			return
	guns[current_weapon][0] -= 1
	if guns[current_weapon][0] < 0:
		kick_player()
	var delta = time_now - last_shot
	if last_shot == -1:
		delta = -1
	if delta != -1:
		if delta < last_cooldown:
			suspicious_activity(check.GUN)
	last_shot = time_now
	last_cooldown = guns[current_weapon][4] * 1000 * DIFF_REL


func reload():
	var time_now = get_time()
	guns[current_weapon][0] = guns[current_weapon][1]
	reload_weapon = current_weapon
	last_cooldown = guns[current_weapon][5] * 1000 * DIFF_REL


func hit(damage):
	var time_now = get_time()
	var delta = time_now - last_hit
	if last_hit == -1:
		delta = -1
	last_hit = time_now
	if current_weapon != "shotgun":
		if delta != -1:
			if delta < last_cooldown:
				suspicious_activity(check.GUN)
			last_cooldown = guns[current_weapon][4] * 1000 * DIFF_REL
	if time_now - last_normal_shot > 2:
		kick_player()
	if damage > guns[current_weapon][6]:
		kick_player()


func die():
	alive = false


func respawn():
	alive = true


func movement_check(old_pos: Vector3, new_pos: Vector3, current_delta, last_delta):
	# Check horizontal speed
	var delta = last_deltas.max()
	var avg_delta = avg(last_deltas)
	var old_pos_h = Vector3(old_pos.x, 0, old_pos.z)
	var new_pos_h = Vector3(new_pos.x, 0, new_pos.z)
	var distance_h = old_pos_h.distance_to(new_pos_h)
	var distance_v = new_pos.y - old_pos.y
	var speed_v = 1000 * distance_v / current_delta
	var speed_h = 1000 * distance_h / avg_delta
	if speed_h > MAX_HORIZONTAL_SPEED + DIFF:
		print("Player moved too fast: %s, delta: %s" % [speed_h, delta])
		suspicious_activity(check.HORIZONTAL_MOVEMENT)
	if speed_v - vertical_velocity > DIFF and get_floor_disctance() > 1:
		print("Player moved incorrectly: actual: %s, should: %s, delta: %s, floor distance: %s" % [speed_v, vertical_velocity, avg_delta, get_floor_disctance()])
		suspicious_activity(check.VERTICAL_MOVEMENT)


func get_time(): 
	return get_node("/root/Multiplayer").time * 1000


func avg(arr: Array) -> float:
	var sum = 0
	for i in arr:
		sum += i
	return sum / arr.size()


func get_floor_disctance():
	if is_on_floor():
		return 0
	$RayCastFeet.force_raycast_update()
	if $RayCastFeet.is_colliding():
		return $RayCastFeet.global_transform.origin.distance_to($RayCastFeet.get_collision_point())
	else:
		return 10


func suspicious_activity(type):
	print("Suspicious activity by player %s" % player_id)
	suspicious_behaviour[type] += 1
	if suspicious_behaviour[type] >= max_suspicion[type]:
		kick_player()


func kick_player():
	print("Kicking player %s." % player_id)
	emit_signal("kick_player", player_id)
