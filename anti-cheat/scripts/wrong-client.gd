extends Node


# This script is part of the anti_cheat.tscn scene, which will only be used if the player
# tries to load the mod with the client instead of the server.
func _ready():
	print("WARNING: The anti-cheat mod does only support servers!")
